﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryConsole_Client
{
    class Item
    {
        public override string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Price { get; set; }
    }
}
